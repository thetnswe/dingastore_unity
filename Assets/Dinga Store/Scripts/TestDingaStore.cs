﻿using UnityEngine;
using System.Collections;
using DingaStore;
using UnityEngine.UI;

public class TestDingaStore : DingaStoreObject
{
    private int balance = 0;
    private string appUDID = "5bf0893d-2621-46c8-b572-85ce98bd3ebb";

    //App Key... Note that the line breaks are also included like below
    private string appKey = @"MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDGSaB9xqLvj+fC
sQbVWNnSudHmcRGX8PefBxsjpo19QrMKMRYManpCL5sNt/OoS/Y8JBfGi6zwB1Qd
2Rh9d5PABE6PAI+R+fndS7+1s7GgKE7cPvgCzeCLRqkQUMFDuxfaoHTva8/5b72Z
yu3CLmwRJMWCPY5jCXqBas+CElBFttgcXbYZQt27ordiL0wV7qQP8BZXewTVRv0D
gaCRFDApwmPG/16ucWcJSeMK7W5NEZhuOWNWv6dHPtyqypRygan2Sf1r4eBkZ8SD
qQSvfhMARk0juiT07A4t8SI7Pp34CYuSmSZnCIVfX6BEg2a2BMZRErxKtyyJTQh0
4KarPaHBAgMBAAECggEAb7nGq3gEENXV8baLsiTJP1oiciTdk+5bqAh5+0F6Uc0G
UsTLtp08XBS02ir/KKH8kXCI0P+cpvaKJ/NOQEvjPXtvxFW9j7P5XP60e65PdH2h
yanVVscoMXHUCvRloUNHMpHcsZbe7Z5Ph9t20kYT8Cj2/OawVw9dLpS/bXviLRTV
7jSOyxBOlAUZMrd0I0YlDsXkxW92NhYcLw9W06WDH3AZpjvUqQO9crQ/wjS17rwA
e0DAMgpNVwZa0blad9PGEtV8OOPjLA4ifikrJA/ZNNx4GoD+yqTLQs8K0iw48/ig
rW06Qu+MIlej1M48ZZKAmgKZWkt21n67W9Oj7dOA0QKBgQDx+3CZkGSK8YPPv4K7
HJK7NBZhlH8SeDe98iyJKMWPi6GREEJIkWjG+FosTti+QbK9Fe5iOX9skUYVxfSL
bM8K7sT7SDlNmgnMLFWUxSWGaARa4O/mNifFJoOs1OxbkZgPXIHMyO+Chk3s64aT
sEy36pyU0Ub64b96OGPzH3NN7QKBgQDRxjPvVG356VAOMFukowS8oR/t9dG8kdeV
KZX+xOO+AyJ1ZhTakgaB8qgqjCjRqFFrgah+hsEv1pd+LWX7e0z2WSBP3zSMjTLa
OxW6YHhxuBZgDXiB+uHVKQxuuqKgywY5+9upnlzKZ0r9h72VcQBpGlAAaPCgVzNA
q6a8QdEIpQKBgQC/hA6JK5O3uBBnsx3ns2SaT05LarjryEPtzKbc420BADUeV6zu
PE+d8l+D4999LQgmyR3Dldg49/1tLCAw3Sw8bd9vNalMttZ8RrZxKVtatiaEOrUE
EeaGDcZ3Wom6q0+bEM8WYg3V0zAHjB0V9JmzkjbdPYkc9OCT/d91Q3YBvQKBgFpk
XdyzNPCLaMO0csjutgvSIkAXvNjUv2C3Grs9fCVUlHSiGDpW0hcMyQoMe0pcjTkW
FdTvnC9Q+NaWGWvUJcXBgfro42vtLAx9iU9WYpAeEwYwx8y6nrWLip/sLEe9NGGn
9W+RncpZ2CaHZ1oN529itzZ2AboV2S/0x1Q3yCtBAoGAc5GQfz/jBiGlORozG3+k
dIyp0wTVmOIquH5vLlZRiA1i9z1ipBFVlH78VaVwuX3C6sfNd+FJH7hdAFfY08q2
mWoJJSNCyazJBozDUmiIQq/EYU201MV342jQjcJAYZ5+EPTo0p8LyQh4nP30BwIv
2/LDpwLIz2BNG7HHrq0veIU=";

    //Test IAP Items
    private string iapItemID01 = "206697ed-5fa3-4cbf-91dd-9cfde13950b0"; //IAP 01 (Non-Consumable)
    private string iapItemID02 = "d9b7566e-ea94-4e80-a30e-8da853ed50ca";  //IAP 02 (Non-Consumable)
    private string iapItemID03 = "9456a118-dded-45f8-98e7-b4d8f42d76a8"; //Multi 01 (Consumables)
    private string iapItemID04 = "858a4262-0e8c-471a-ba05-36d010ebea3f";  //Duration 01 (Duration)
    private string iapItemID05 = "0f116ffe-7b17-44ce-b70b-e0f3ddb28750"; //Expiry 01 (Expiry Date)

    private bool isDingaInstalled = false, isDingaInitialised = false;

    //Buttons
    public UI_Button btnInitialise, btnCheckStore, btnLaunchStore;
    public UI_Button btnEncrypt, btnDecrypt;
    public Text txtInitialiseStatus, txtInstalledStatus, txtEncryptionText;

    void Start()
    {
        btnInitialise.AddEventListener(gameObject, "onInitialise");
        btnCheckStore.AddEventListener(gameObject, "onCheckStore");
        btnLaunchStore.AddEventListener(gameObject, "onLaunchStore");

        btnEncrypt.AddEventListener(gameObject, "onEncrypt");
        btnDecrypt.AddEventListener(gameObject, "onDecrypt");


        btnCheckStore.SetInteractable(false);
        btnLaunchStore.SetInteractable(false);


    }

    //Button Callbacks

    //Initialise Dinga store DRM system
    void onInitialise()
    {
        //Initialise Dinga store with appkey and app's UDID
        DingaStorePlugin.InitialiseDRM(appKey, appUDID);

        //Check whether the initialisation successful or not
        isDingaInitialised = DingaStorePlugin.IsDingaInitialised();

        if(isDingaInitialised)
        {
            btnCheckStore.SetInteractable(true);
            btnLaunchStore.SetInteractable(true);

            //Add IAP Items after initialisation done
            DingaStorePlugin.AddIAP(iapItemID01);
            DingaStorePlugin.AddIAP(iapItemID02);
            DingaStorePlugin.AddIAP(iapItemID03);
            DingaStorePlugin.AddIAP(iapItemID04);
            DingaStorePlugin.AddIAP(iapItemID05);
        }
        else
        {
            Debug.Log("Something wrong while initialisation Dinga Store plugins.");
        }

        //Update the status Text
        txtInitialiseStatus.text = "Initialisation status : " + isDingaInitialised;
    }

    //TODO: Write encryption and decryption of strings
    void onEncrypt()
    {
        DingaStorePlugin.Decrypt("876345jhdfgkh9");
    }

    void onDecrypt()
    {
        DingaStorePlugin.Decrypt("2398^(090237lk(*PJ)(&*");
    }

    //Check whether the Dinga store app is already installed in the system
    void onCheckStore()
    {
        isDingaInstalled = DingaStorePlugin.IsDingaInstalled();
        txtInstalledStatus.text = "Iinstalled status : " + isDingaInstalled;
    }

    //Launch the dingastore installer if it's not installed yet in the system
    void onLaunchStore()
    {
        isDingaInstalled = DingaStorePlugin.IsDingaInstalled();

        if (!isDingaInstalled)
            DingaStorePlugin.LaunchDingaStoreInstaller();
        else
            Debug.Log("Dingastore already installed in the system");
    }

    //Get the Dinga store events
    protected override void OnInitialisationEvent(DingaStoreEventType type, string strMessage)
    {
        DingaStorePlugin.ShowToast("DRM not initialised yet. Please call init function first.");
    }
}
