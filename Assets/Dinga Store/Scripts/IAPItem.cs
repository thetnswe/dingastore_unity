﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DingaStore;

public class IAPItem : DingaStoreObject 
{
    public string iapItemName = "Non Consumable 01";
    public string iapItemID = "";
    public bool isIAP = true;
    public Text txtBoughtStatus;
    public int requestCode = 0;
    public int purchaseIndex = 0;
    public UI_Button btnBuy, btnCheck, btnDetails;

	// Use this for initialization
	void Start () 
    {
        btnBuy.AddEventListener(gameObject, "onBuy");
        btnCheck.AddEventListener(gameObject, "onCheck");
        btnDetails.AddEventListener(gameObject, "onDetails");
	}
	
    void onBuy()
    {
        if (isIAP)
            DingaStorePlugin.PurchaseIAP(iapItemID, requestCode);
        else
            DingaStorePlugin.PurchaseApp(requestCode);
	}

    void onCheck()
    {
        if (isIAP)
            DingaStorePlugin.IsIapPurchased(iapItemID);
        else
            DingaStorePlugin.IsAppPurchased();
    }

    void onDetails()
    {
        string strPayload = "";
        if (isIAP)
        {
            strPayload= DingaStorePlugin.GetPayloadIAP(iapItemID, purchaseIndex);
        }
        else
        {
            strPayload = DingaStorePlugin.GetPayload(purchaseIndex);
        }

        Debug.Log(strPayload);
    }

    //private string strPurchaseStatus = "Listening", strTransferStatus = "Listening", strRequestBalanceStatus = "Listening";
    protected override void OnPurchaseEvent(DingaStoreEventType type, int requestCode)
    {
        Debug.Log(string.Format("Purchase Event : {0} with the item id of {1}", type, iapItemID));

        if (type == DingaStoreEventType.Successful)
        {
            if (this.requestCode == requestCode)
            {
                txtBoughtStatus.text = "Is Bought? : True";
                DingaStorePlugin.ShowToast(iapItemName + " bought successfuly.");
            }
        }
        else
        {
            txtBoughtStatus.text = "Is Bought? : False";
            Debug.Log("Item failed to purchase for some reason : " + iapItemID + " : " + iapItemID);
            DingaStorePlugin.ShowToast(iapItemName + " failed to purchase.");
        }
    }

    protected override void OnCheckPurchaseAppEvent(DingaStoreEventType type, string strMessage)
    {
        if (isIAP || strMessage == null)
            return;

        Debug.Log(string.Format("Check Purchase App Event : {0} with the message of {1}", type, strMessage));

        if (type == DingaStoreEventType.Successful)
        {
            txtBoughtStatus.text = "Is Bought? : True";
            DingaStorePlugin.ShowToast(iapItemName + " already bought.");
        }
    }

    protected override void OnCheckPurchaseIapEvent(DingaStoreEventType type, string iapItemID)
    {
        if (!isIAP || iapItemID == null || this.iapItemID != iapItemID)
            return;

        //Also double check with the class's iapItemID to make sure that it's the one that we just purchased.
        if (type == DingaStoreEventType.Successful)
        {
            txtBoughtStatus.text = "Is Bought? : True";
            DingaStorePlugin.ShowToast(iapItemName + " already bought.");
        }
        else
        {
            DingaStorePlugin.ShowToast(iapItemName + " not bought yet.");
        }

        Debug.Log(string.Format("Check Purchase IAP Event : {0} with the request code of {1}", type, iapItemID));
    }
}
