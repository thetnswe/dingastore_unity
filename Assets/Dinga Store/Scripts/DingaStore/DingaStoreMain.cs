﻿using UnityEngine;
using System.Collections;

namespace DingaStore
{
    public class DingaStoreMain
    {
        protected static AndroidJavaObject _androidPlugin;
        protected static AndroidJavaClass _androidStaticClass;
        protected static AndroidJavaObject activity;

        //----------------------------------------------------------------
        //Utility Function to call native android methods in UI thread
        //----------------------------------------------------------------
        //Running on UI thread sometimes doesn't get the data back

        protected static void CallJavaMethodOnUiThread(string methodName, params object[] args)
        {
            if (Application.platform != RuntimePlatform.Android)
                return;

            activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                _androidPlugin.Call(methodName, args);
            }));
        }

        protected static void CallJavaMethod(string methodName, params object[] args)
        {
            if (Application.platform != RuntimePlatform.Android)
                return;

            _androidPlugin.Call(methodName, args);
        }

        protected static string CallJavaMethodReturnString(string methodName, params object[] args)
        {
            if (Application.platform != RuntimePlatform.Android)
                return "1.0";

            string bNum = "1.0";
            bNum = _androidPlugin.Call<string>(methodName, args);

            return bNum;
        }
        protected static bool CallJavaMethodReturnBoolean(string methodName, params object[] args)
        {
            if (Application.platform != RuntimePlatform.Android)
                return false;

            bool bCorrect = false;
            bCorrect = _androidPlugin.Call<bool>(methodName, args);
            return bCorrect;

            //Run on UI Code from Unity3D C# Side.. it can also be done in Java side as well
            //activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            //{
            //    try
            //    {
            //        bCorrect = _androidPlugin.Call<bool>(methodName, args);
            //    }
            //    catch (System.Exception ex)
            //    {
            //        Debug.Log("Error : " + ex.Message);
            //    }
            //}));

            //return bCorrect;
        }
        protected static double CallJavaMethodReturnDouble(string methodName, params object[] args)
        {
            if (Application.platform != RuntimePlatform.Android)
                return 1.0;

            double bNum = 0.0;
            bNum = _androidPlugin.Call<double>(methodName, args);

            return bNum;
        }
        protected static int CallJavaMethodReturnInt(string methodName, params object[] args)
        {
            if (Application.platform != RuntimePlatform.Android)
                return 1;

            int bNum = 0;
            bNum = _androidPlugin.Call<int>(methodName, args);

            return bNum;
        }
    }
}