﻿using UnityEngine;
using System.Collections;

namespace DingaStore
{
    public class DingaStoreManager : MonoBehaviour
    {
        private void OnDRMInitialised(string strMessage)
        {
            if (strMessage == "SUCCESSFUL")
                DingaStoreEvents.TriggerInitialisationEvent(DingaStoreEventType.Successful, strMessage);
            else
                DingaStoreEvents.TriggerInitialisationEvent(DingaStoreEventType.Failed, strMessage);          
        }

        //Message return from java plugs whether the App purchased is SUCCESSFUL or FAILED
        private void OnCheckAppPurchased(string strMessage)
        {
            if (strMessage == "SUCCESSFUL")
                DingaStoreEvents.TriggerCheckPurchaseAppEvent(DingaStoreEventType.Successful, strMessage);
            else
                DingaStoreEvents.TriggerCheckPurchaseAppEvent(DingaStoreEventType.Failed, strMessage);            
        }

        //Message return from java plugs whether the IAP purchased is SUCCESSFUL or FAILED
        private void OnCheckIapPurchased(string strMessage)
        {
            string[] infos = strMessage.Split(',');

            if (infos.Length < 2)
                DingaStoreEvents.TriggerCheckPurchaseIapEvent(DingaStoreEventType.Failed, "Failed to purchase the IAP item for unknown reason : " + strMessage);

            if (infos[0] == "SUCCESSFUL")
                DingaStoreEvents.TriggerCheckPurchaseIapEvent(DingaStoreEventType.Successful, infos[1]);
            else
                DingaStoreEvents.TriggerCheckPurchaseIapEvent(DingaStoreEventType.Failed, infos[1]);            
        }

        //If the IAP Item is purchased, return trhe approprate message along with the purchased IAP Item ID
        private void OnPurchased(string strMessage)
        {
            Debug.Log("Purchased message received : " + strMessage);
            string[] infos = strMessage.Split(',');

            if(infos.Length < 2)
                DingaStoreEvents.TriggerPurchaseEvent(DingaStoreEventType.Failed, "Failed to purchase the IAP item for unknown reason : " + strMessage); 

            switch(infos[0])
            {
                case "SUCCESSFUL" : DingaStoreEvents.TriggerPurchaseEvent(DingaStoreEventType.Successful, infos[1]); break; //Send Request Code as infos[1]
                case "FAILED": DingaStoreEvents.TriggerPurchaseEvent(DingaStoreEventType.Failed, infos[1]); break;//Send Request Code as infos[1]
            }
        }
    }
}
