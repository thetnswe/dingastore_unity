﻿using UnityEngine;
using System.Collections;

namespace DingaStore
{
    public class DingaStoreObject : MonoBehaviour
    {
        protected virtual void OnScriptEnable() { }
        protected virtual void OnScriptDisable() { }

        // Use this for initialization
        void OnEnable()
        {

            DingaStoreEvents.initialisationEvent += OnInitialisationEvent;
            DingaStoreEvents.purchaseEvent += OnPurchaseEvent;
            DingaStoreEvents.checkPurchaseAppEvent += OnCheckPurchaseAppEvent;
            DingaStoreEvents.checkPurchaseIapEvent += OnCheckPurchaseIapEvent;
            
            OnScriptEnable();
        }

        // Update is called once per frame
        void OnDisable()
        {
            DingaStoreEvents.initialisationEvent -= OnInitialisationEvent;
            DingaStoreEvents.purchaseEvent -= OnPurchaseEvent;
            DingaStoreEvents.checkPurchaseAppEvent -= OnCheckPurchaseAppEvent;
            DingaStoreEvents.checkPurchaseIapEvent -= OnCheckPurchaseIapEvent;

            OnScriptDisable();
        }

        protected virtual void OnInitialisationEvent(DingaStoreEventType type, string strMessage)
        {
            Debug.Log(string.Format("Initialisation DRM Event : {0} with the request code of {1}", type, strMessage));
        }

        protected virtual void OnPurchaseEvent(DingaStoreEventType type, int requestCode)
        {
            Debug.Log(string.Format("Purchase Event : {0} with the request code of {1}", type, requestCode));
        }

        protected virtual void OnCheckPurchaseAppEvent(DingaStoreEventType type, string strMessage)
        {
            Debug.Log(string.Format("Purchase App Event : {0} with the request code of {1}", type, strMessage));
        }

        protected virtual void OnCheckPurchaseIapEvent(DingaStoreEventType type, string iapItemID)
        {
            Debug.Log(string.Format("Purchase IAP Event : {0} with the request code of {1}", type, iapItemID));
        }

    }
}
