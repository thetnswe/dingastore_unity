﻿using UnityEngine;
using System.Collections;

#if UNITY_IOS
    // We need this one for importing our IOS functions
    using System.Runtime.InteropServices;
#endif

namespace DingaStore
{
    public class DingaStorePlugin : DingaStoreMain
    {
#if UNITY_ANDROID
        public static void InitialiseDRM(string appKey, string appID)
        {
            if (Application.platform != RuntimePlatform.Android)
                return;

            //Get the instance of the Plugin class
            using (var pluginClass = new AndroidJavaClass("com.JoyDash.DingaStorePlugins.DingaStorePlugin"))
                _androidPlugin = pluginClass.CallStatic<AndroidJavaObject>("instance");

            //Get the current Unity player activity in the Android
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            //Get the static class
            AndroidJavaClass _androidStaticClass = new AndroidJavaClass("com.JoyDash.DingaStorePlugins.DingaStorePlugin");
            
            //Initialise the Dingastore
            _androidPlugin.Call("InitialiseDRM", appKey, appID);
        }

        public static void ShowToast(string message) { CallJavaMethodOnUiThread("ShowToast", message); }
        public static bool IsDingaInstalled() { return CallJavaMethodReturnBoolean("IsDingaInstalled"); }
        public static bool IsDingaInitialised() { return CallJavaMethodReturnBoolean("IsDingaInitialised"); }        
        public static void LaunchDingaStoreInstaller() { CallJavaMethod("LaunchDingaStoreInstaller"); }
        public static void AddIAP(string iapItemID) { CallJavaMethod("AddIAP", iapItemID); }
        public static void IsIapPurchased(string iapItemID) { CallJavaMethod("IsIAPPurchased", iapItemID); } //Return via Message including Result type and request Code
        public static void PurchaseIAP(string iapItemID, int requestCode) { CallJavaMethod("PurchaseIAP", iapItemID, requestCode); }

        public static void IsAppPurchased() { CallJavaMethod("IsAppPurchased"); } //Return via Message including Result type and request Code
        public static void PurchaseApp(int requestCode) { CallJavaMethod("PurchaseApp",  requestCode); }

        public static string GetPayload(int purchaseIndex) { return CallJavaMethodReturnString("GetPayload", purchaseIndex); }
        public static string GetPayloadIAP(string iapItemID, int purchaseIndex) { return CallJavaMethodReturnString("GetPayloadIAP", iapItemID, purchaseIndex); }

        public static string Decrypt(string cipher) { return CallJavaMethodReturnString("Decrypt", cipher); }
        public static string DecryptIAP(string cipher, string iapItemID) { return CallJavaMethodReturnString("Decrypt", cipher, iapItemID); }

        //Dinga Store Plugins Main calls
        public static string TestCall(string testParam) { return CallJavaMethodReturnString("TestCall", testParam); }
   
#endif

    }
}