﻿using UnityEngine;
using System;
using System.Collections;

namespace DingaStore
{
    public enum DingaStoreEventType
    {
        //Purchases
        Successful, Illegal, Failed, Unknown,
    }

    public class DingaStoreEvents : MonoBehaviour
    {
        public static event Action<DingaStoreEventType, string> initialisationEvent;//return normal Message (SUCCESSFUL or FAILED)
        public static event Action<DingaStoreEventType, int> purchaseEvent;//return requestCode
        public static event Action<DingaStoreEventType, string> checkPurchaseAppEvent;//return normal message (SUCCESSFUL or FAILED)
        public static event Action<DingaStoreEventType, string> checkPurchaseIapEvent; //return iapItemID

        #region Trigger Events
        public static void TriggerInitialisationEvent(DingaStoreEventType eventType, string strMessage)
        {
            if (initialisationEvent != null)
                initialisationEvent(eventType, strMessage);
        }

        public static void TriggerPurchaseEvent(DingaStoreEventType eventType, string strRequestCode)
        {
            int requestCode = 0;
            if (int.TryParse(strRequestCode, out requestCode))
            {
                if (purchaseEvent != null)
                    purchaseEvent(eventType, requestCode);
            }
            else
            {
                Debug.Log("Something with request code in TriggerPurchaseEvent.");
            }
        }
        public static void TriggerCheckPurchaseAppEvent(DingaStoreEventType eventType, string strMessage)
        {
            if (checkPurchaseAppEvent != null)
                checkPurchaseAppEvent(eventType, strMessage);
        }
        public static void TriggerCheckPurchaseIapEvent(DingaStoreEventType eventType, string iapItemID)
        {
            if (checkPurchaseIapEvent != null)
                checkPurchaseIapEvent(eventType, iapItemID);

        }
        #endregion
    }
}