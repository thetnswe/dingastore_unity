﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UI_Button : MonoBehaviour 
{
    private Button btnMain;
    private GameObject listener;
    private string message = "";

	// Use this for initialization
    void Start()
    {
        //Debug.Log("Assigning Buttons");
        if (btnMain == null)
            btnMain = GetComponent<Button>();

        if (btnMain != null)
            btnMain.onClick.AddListener(() => { onClicked(); });
        else
            Debug.Log("No Button component existed yet in the game object : " + this.name);
	}

    //Enable/Disable the button
    public void SetInteractable(bool val)
    {
        if (btnMain == null)
            btnMain = GetComponent<Button>();

        if (btnMain)
            btnMain.interactable = val;
    }

    //Code to add the listener for callback message once the button is clicked
    public void AddEventListener(GameObject listener, string message)
    {
        this.listener = listener;
        this.message = message;
    }

    protected virtual void onClicked()
    {
        if (listener != null)
            listener.SendMessage(message, SendMessageOptions.DontRequireReceiver);
        else
            Debug.Log("Button require to add event listener : " + this.name);
    }

}
